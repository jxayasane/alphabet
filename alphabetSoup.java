import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class alphabetSoup 
{

    public static void main(String[] args) 
    {
        //replace WordSearch with filename
        try (BufferedReader bufferedread = new BufferedReader(new FileReader("src/WordSearch")))
        {
            // Read the grid size
            String gridSizeLine = bufferedread.readLine();
            String[] gridSizeParts = gridSizeLine.split("x");
            int rows = Integer.parseInt(gridSizeParts[0]);
            int cols = Integer.parseInt(gridSizeParts[1]);

            // Read the character grid
            char[][] grid = new char[rows][cols];
            for (int i = 0; i < rows; i++) 
            {
                String gridRow = bufferedread.readLine().replaceAll(" ", "");
                grid[i] = gridRow.toCharArray(); //converts grid to a char array
            }

            // Read the words to find
            String word;
            while ((word = bufferedread.readLine()) != null) //while there are still lines to read
            { 
                searchprintWord(grid, word); //searches and prints word
            }
        }
        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }

    public static void searchprintWord(char[][] grid, String word) 
    {
        int rows = grid.length;
        int cols = grid[0].length;
        int wordLen = word.length();

        for (int r = 0; r < rows; r++) //goes through every row 
        {
            for (int c = 0; c < cols; c++) //goes through every column
            {
                for (int otherRow = -1; otherRow <= 1; otherRow++) //checks in every direction (horizontally, vertically, and diagnonally)
                {
                    for (int otherCol = -1; otherCol <= 1; otherCol++) 
                    {
                        if (otherCol == 0 && otherRow == 0) continue;

                        int endRow = r + (wordLen - 1) * otherRow;
                        int endCol = c + (wordLen - 1) * otherCol;

                        if (endCol >= 0 && endCol < cols && endRow >= 0 && endRow < rows) 
                        {
                            boolean found = true;
                            for (int i = 0; i < wordLen; i++) 
                            {
                                int rCurr = r + i * otherRow;
                                int cCurr = c + i * otherCol;
                                if (grid[rCurr][cCurr] != word.charAt(i)) 
                                {
                                    found = false;
                                    break;
                                }
                            }

                            if (found) 
                            {
                                System.out.printf("%s %d:%d %d:%d%n", word, r, c, endRow, endCol);
                                return;
                            }
                        }
                    }
                }
            }
        }
    }
}

